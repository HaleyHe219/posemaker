package pm.data;

import java.util.ArrayList;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import saf.components.AppDataComponent;
import saf.AppTemplate;
import pm.file.FileManager;
import pm.gui.Workspace;

/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class DataManager implements AppDataComponent {
     // THIS FILE HAS THE LIST OF TAGS OUR APPLICATION WILL USE
    static final String TAG_TYPES_FILE_PATH = "data/tags.json";
    
    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;
    
    String currentSetting;
    Color background;
    
    ArrayList<Shape> shapes;


    
    Shape selectedShape;
    Color selectedOutline;
    double selectedThickness;
    
    double selectedSceneX;
    double selectedSceneY;
    
    /**
     * THis constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public DataManager(AppTemplate initApp) throws Exception {
	// KEEP THE APP FOR LATER
	app = initApp;
    }
    
    /**
     * Mutator for the app setting
     * @param newSetting the new setting of the app
     */
    public void setSetting (String newSetting) {
        currentSetting = newSetting;
    }
    
    /**
     * Accessor for the app setting
     * @return the current setting of the app
     */
    public String getSetting () {
        return currentSetting;
    }
    
    public ArrayList<Shape> getShapesArray () {
        return shapes;
    }
    
    public int getShapesIndex(Shape shape) {
        return shapes.indexOf(shape);
    }
    
    public void addShape (Shape newShape) {
        shapes.add(newShape);
    }
    
    public void setShape (int index, Shape newShape) {
        shapes.set(index, newShape);
    }
    
    public void removeShape (Shape shape) {
        shapes.remove(shape);
    }
    
    public void moveToFront (Shape shape) {
        int index = shapes.indexOf(shape);
        shapes.remove(index);
        shapes.add(shape);
    }
    
    public void moveToBack (Shape shape) {
        int index = shapes.indexOf(shape);
        for (int i=index-1; i >= 0; i--) 
            shapes.set(i+1, shapes.get(i));
        shapes.set(0, shape);
    }
    
    public void makeShapesArray (int size) {
        shapes = new ArrayList<Shape> (size);
    }
    
    public Shape getSelectedShape () {
        return selectedShape;
    }
    
    public void setSelectedShape (Shape newSelected) {
        selectedShape = newSelected;
    }
    
    public Color getSelectedOutline () {
        return selectedOutline;
    }
    
    public void setSelectedOutline (Color newOutline) {
        selectedOutline = newOutline;
    }
    
    public double getSelectedThickness () {
        return selectedThickness;
    }
    
    public void setSelectedThickness (double newThickness) {
        selectedThickness = newThickness;
    }
    
    public double getSelectedSceneX() {
        return selectedSceneX;
    }
    
    public void setSelectedSceneX(double x) {
        selectedSceneX = x;
    }
    
    public double getSelectedSceneY() {
        return selectedSceneY;
    }
    
    public void setSelectedSceneY(double y) {
        selectedSceneY = y;
    }
    
    public Color getBackground () {
        return background;
    }
    
    public void setBackground (Color newColor) {
        background = newColor;
    }
    
    /**
     * 
     */
    @Override
    public void reset() {
        //remove all of the shapes from the canvas
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        if (workspace.getCanvas().getChildren().size() > 0) {
            workspace.getCanvas().getChildren().removeAll(shapes);
        }
        shapes = new ArrayList<Shape>();
        currentSetting = "none";
        selectedShape = null;
        background = Color.GAINSBORO;
    }
}
