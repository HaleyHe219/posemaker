package pm.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.scene.Cursor;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import pm.PoseMaker;
import pm.controller.PageEditController;
import pm.data.DataManager;
import pm.gui.Workspace;
import saf.AppTemplate;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;

/**
 * This class serves as the file management component for this application,
 * providing all I/O services.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class FileManager implements AppFileComponent {
    static final String JSON_BACKGROUND_R = "background_r";
    static final String JSON_BACKGROUND_B = "background_b";
    static final String JSON_BACKGROUND_G = "background_g";
    static final String JSON_BACKGROUND_O = "background_opacity";
    static final String JSON_APP_SETTING = "setting";
    static final String JSON_RECT = "rectangle";
    static final String JSON_ELLIPSE = "ellipse";

    static final String JSON_SHAPE = "shapes";
    static final String JSON_SHAPE_TYPE = "shape_type";
    static final String JSON_RECT_X = "corner_x";
    static final String JSON_RECT_Y = "corner_y";
    static final String JSON_RECT_WIDTH = "width";
    static final String JSON_RECT_HEIGHT = "height";
    static final String JSON_ELLIPSE_X = "center_x";
    static final String JSON_ELLIPSE_Y = "center_y";
    static final String JSON_ELLIPSE_RADX = "radius_x";
    static final String JSON_ELLIPSE_RADY = "radius_y";
    static final String JSON_SHAPE_FILL_R = "fill_r";
    static final String JSON_SHAPE_FILL_B = "fill_b";
    static final String JSON_SHAPE_FILL_G = "fill_g";
    static final String JSON_SHAPE_FILL_OPACITY = "fill_opacity";
    static final String JSON_SHAPE_OUTLINE_R = "outline_color_r";
    static final String JSON_SHAPE_OUTLINE_B = "outline_color_b";
    static final String JSON_SHAPE_OUTLINE_G = "outline_color_g";
    static final String JSON_SHAPE_OUTLINE_OPACITY = "outline_color_opacity";
    static final String JSON_SHAPE_OUTLINE_THICKNESS = "outline_thickness";
    static final String JSON_SHAPE_ARRAY_INDEX = "shape_array_index";
    static final String JSON_SHAPE_ARRAY_SIZE = "shape_array_size";
    
    int shapeCount;

    /**
     * This method is for saving user work, which in the case of this
     * application means the data that constitutes the page DOM.
     * 
     * @param data The data management component for this application.
     * 
     * @param filePath Path (including file name/extension) to where
     * to save the data to.
     * 
     * @throws IOException Thrown should there be an error writing 
     * out data to the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
            StringWriter sw = new StringWriter();
            DataManager dataM = (DataManager) data;
            
            //temporarily deselect the shape
            Shape selected = dataM.getSelectedShape();
            if (selected != null) {
                selected.setStroke(dataM.getSelectedOutline());
                selected.setStrokeWidth(dataM.getSelectedThickness());
            }
            
            ArrayList<Shape> shapes = dataM.getShapesArray();
            JsonArrayBuilder shapesBuilder = Json.createArrayBuilder();
            for (Shape s : shapes) {
                if (s.getClass().equals(new Rectangle().getClass()))
                    shapesBuilder.add(makeRectJsonObject((Rectangle) s, dataM));
                else if (s.getClass().equals(new Ellipse().getClass()))
                    shapesBuilder.add(makeEllipseJsonObject((Ellipse) s, dataM));
            }
            JsonArray shapesArray = shapesBuilder.build();
            
            JsonObject dataManagerJSO = Json.createObjectBuilder()
                    //.add(JSON_APP_SETTING, dataM.getSetting())
                    .add(JSON_BACKGROUND_R, dataM.getBackground().getRed())
                    .add(JSON_BACKGROUND_B, dataM.getBackground().getBlue())
                    .add(JSON_BACKGROUND_G, dataM.getBackground().getGreen())
                    .add(JSON_BACKGROUND_O, dataM.getBackground().getOpacity())
                    //.add(JSON_RECT, rectArray)
                    //.add(JSON_ELLIPSE, ellipseArray)
                    .add(JSON_SHAPE, shapesArray)
                    .add(JSON_SHAPE_ARRAY_SIZE, dataM.getShapesArray().size())
                    .build();
                       
        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
        
        //reselect the shape
        if (selected != null) {
            selected.setStroke(Color.YELLOW);
            if (selected.getStrokeWidth() <= 1)
                selected.setStrokeWidth(20);
        }
    }
    
    private JsonObject makeRectJsonObject (Rectangle rect, DataManager dataM) {
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_SHAPE_TYPE, JSON_RECT)
                .add(JSON_RECT_X, rect.getX())
                .add(JSON_RECT_Y, rect.getY())
                .add(JSON_RECT_WIDTH, rect.getWidth())
                .add(JSON_RECT_HEIGHT, rect.getHeight())
                .add(JSON_SHAPE_FILL_R, ((Color) rect.getFill()).getRed())
                .add(JSON_SHAPE_FILL_B, ((Color) rect.getFill()).getBlue())
                .add(JSON_SHAPE_FILL_G, ((Color) rect.getFill()).getGreen())
                .add(JSON_SHAPE_FILL_OPACITY, ((Color) rect.getFill()).getOpacity())
                .add(JSON_SHAPE_OUTLINE_R, ((Color) rect.getStroke()).getRed())
                .add(JSON_SHAPE_OUTLINE_B, ((Color) rect.getStroke()).getBlue())
                .add(JSON_SHAPE_OUTLINE_G, ((Color) rect.getStroke()).getGreen())
                .add(JSON_SHAPE_OUTLINE_OPACITY, ((Color) rect.getStroke()).getOpacity())
                .add(JSON_SHAPE_OUTLINE_THICKNESS, rect.getStrokeWidth())
                .add(JSON_SHAPE_ARRAY_INDEX, dataM.getShapesIndex(rect))
                .build();
        return jso;
    }
    
    private JsonObject makeEllipseJsonObject (Ellipse ellipse, DataManager dataM) {
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_SHAPE_TYPE, JSON_ELLIPSE)
                .add(JSON_ELLIPSE_X, ellipse.getCenterX())
                .add(JSON_ELLIPSE_Y, ellipse.getCenterY())
                .add(JSON_ELLIPSE_RADX, ellipse.getRadiusX())
                .add(JSON_ELLIPSE_RADY, ellipse.getRadiusY())
                .add(JSON_SHAPE_FILL_R, ((Color) ellipse.getFill()).getRed())
                .add(JSON_SHAPE_FILL_B, ((Color) ellipse.getFill()).getBlue())
                .add(JSON_SHAPE_FILL_G, ((Color) ellipse.getFill()).getGreen())
                .add(JSON_SHAPE_FILL_OPACITY, ((Color) ellipse.getFill()).getOpacity())
                .add(JSON_SHAPE_OUTLINE_R, ((Color) ellipse.getStroke()).getRed())
                .add(JSON_SHAPE_OUTLINE_B, ((Color) ellipse.getStroke()).getBlue())
                .add(JSON_SHAPE_OUTLINE_G, ((Color) ellipse.getStroke()).getGreen())
                .add(JSON_SHAPE_OUTLINE_OPACITY, ((Color) ellipse.getStroke()).getOpacity())
                .add(JSON_SHAPE_OUTLINE_THICKNESS, ellipse.getStrokeWidth())
                .add(JSON_SHAPE_ARRAY_INDEX, dataM.getShapesIndex(ellipse))
                .build();
        return jso;
    }

    /**
     * This method loads data from a JSON formatted file into the data 
     * management component and then forces the updating of the workspace
     * such that the user may edit the data.
     * 
     * @param data Data management component where we'll load the file into.
     * 
     * @param filePath Path (including file name/extension) to where
     * to load the data from.
     * 
     * @throws IOException Thrown should there be an error reading
     * in data from the file.
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        // CLEAR THE OLD DATA OUT
	DataManager dataM = (DataManager)data;
	dataM.reset();
        
        // LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
        
        //LOAD THE BACKGROUND COLOR
        Color bgcolor = reconstructColor(json, JSON_BACKGROUND_R, JSON_BACKGROUND_G, 
                JSON_BACKGROUND_B, JSON_BACKGROUND_O);
        dataM.setBackground(bgcolor);
               
        /*
        //LOAD THE RECT ARRAY
        JsonArray rectArray = json.getJsonArray(JSON_RECT);
        loadRectangles(rectArray, dataM);
        
        //LOAD THE ELLIPSE ARRAY
        JsonArray ellipseArray = json.getJsonArray(JSON_ELLIPSE);
        loadEllipses(ellipseArray, dataM);
        */
        
        //LOAD THE SHAPES ARRAY
        JsonArray shapeArray = json.getJsonArray(JSON_SHAPE);
        loadShapes(shapeArray, dataM);
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    private Color reconstructColor(JsonObject jo, String redID, String greenID, String blueID, String opacityID) {
        double r = (double) jo.getJsonNumber(redID).doubleValue();
        double b = (double) jo.getJsonNumber(blueID).doubleValue();
        double g = (double) jo.getJsonNumber(greenID).doubleValue();
        double opacity = (double) jo.getJsonNumber(opacityID).doubleValue();
        Color color = new Color (r,g,b,opacity);
        return color;
    }
    
    private void loadShapes (JsonArray jsonShapeArray, DataManager dataM) {
        for (int i = 0; i < jsonShapeArray.size(); i++) { 
              JsonObject shapeJso = jsonShapeArray.getJsonObject(i);
              if (shapeJso.getString(JSON_SHAPE_TYPE).equals(JSON_RECT))
                 loadRectangle(shapeJso, dataM);
              else if (shapeJso.getString(JSON_SHAPE_TYPE).equals(JSON_ELLIPSE))
                 loadEllipse(shapeJso, dataM);
        }
    }
    
    private void loadRectangle (JsonObject rectJso, DataManager dataM) {
	    double x = rectJso.getJsonNumber(JSON_RECT_X).doubleValue();
            double y = rectJso.getJsonNumber(JSON_RECT_Y).doubleValue();
            double width = rectJso.getJsonNumber(JSON_RECT_WIDTH).doubleValue();
            double height = rectJso.getJsonNumber(JSON_RECT_HEIGHT).doubleValue();
            Rectangle r = new Rectangle(x, y, width, height);
            Color fillColor = reconstructColor(rectJso, JSON_SHAPE_FILL_R, JSON_SHAPE_FILL_G, 
                JSON_SHAPE_FILL_B, JSON_SHAPE_FILL_OPACITY);
            r.setFill(fillColor);
            Color outlineColor = reconstructColor(rectJso, JSON_SHAPE_OUTLINE_R, JSON_SHAPE_OUTLINE_G, 
                JSON_SHAPE_OUTLINE_B, JSON_SHAPE_OUTLINE_OPACITY);
            r.setStroke(outlineColor);
            double thickness = rectJso.getJsonNumber(JSON_SHAPE_OUTLINE_THICKNESS).doubleValue();
            r.setStrokeWidth(thickness);
            dataM.addShape(r);
    }
    
        private void loadEllipse (JsonObject ellipseJso, DataManager dataM) {
	    double x = ellipseJso.getJsonNumber(JSON_ELLIPSE_X).doubleValue();
            double y = ellipseJso.getJsonNumber(JSON_ELLIPSE_Y).doubleValue();
            double radX = ellipseJso.getJsonNumber(JSON_ELLIPSE_RADX).doubleValue();
            double radY = ellipseJso.getJsonNumber(JSON_ELLIPSE_RADY).doubleValue();
            Ellipse ellipse = new Ellipse(x,y,radX,radY);
            Color fillColor = reconstructColor(ellipseJso, JSON_SHAPE_FILL_R, JSON_SHAPE_FILL_G, 
                JSON_SHAPE_FILL_B, JSON_SHAPE_FILL_OPACITY);
            ellipse.setFill(fillColor);
            Color outlineColor = reconstructColor(ellipseJso, JSON_SHAPE_OUTLINE_R, JSON_SHAPE_OUTLINE_G, 
                JSON_SHAPE_OUTLINE_B, JSON_SHAPE_OUTLINE_OPACITY);
            ellipse.setStroke(outlineColor);
            double thickness = ellipseJso.getJsonNumber(JSON_SHAPE_OUTLINE_THICKNESS).doubleValue();
            ellipse.setStrokeWidth(thickness);
            dataM.addShape(ellipse);
    }
    
    /**
     * This method exports the contents of the data manager to a 
     * Web page including the html page, needed directories, and
     * the CSS file.
     * 
     * @param data The data management component.
     * 
     * @param filePath Path (including file name/extension) to where
     * to export the page to.
     * 
     * @throws IOException Thrown should there be an error writing
     * out data to the file.
     */
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {

    }
    
    /**
     * This method is provided to satisfy the compiler, but it
     * is not used by this application.
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
	// NOTE THAT THE Web Page Maker APPLICATION MAKES
	// NO USE OF THIS METHOD SINCE IT NEVER IMPORTS
	// EXPORTED WEB PAGES
    }
}
