/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javax.imageio.ImageIO;
import pm.PoseMaker;
import static pm.PropertyType.SNAPSHOT_ERROR_MESSAGE;
import static pm.PropertyType.SNAPSHOT_ERROR_TITLE;
import pm.data.DataManager;
import pm.file.FileManager;
//import static wpm.file.FileManager.TEMP_CSS_PATH;
//import static wpm.file.FileManager.TEMP_PAGE;
import pm.gui.Workspace;
import properties_manager.PropertiesManager;
import saf.ui.AppMessageDialogSingleton;

/**
 *
 * @author haley.he
 */
public class PageEditController {
     PoseMaker app;
     private boolean enabled;
     
    
     /**
     * Constructor for initializing this object, it will keep the app for later.
     *
     * @param initApp The JavaFX application this controller is associated with.
     */
    public PageEditController(PoseMaker initApp) {
	// KEEP IT FOR LATER
	app = initApp;
    }

    /**
     * This mutator method lets us enable or disable this controller.
     *
     * @param enableSetting If false, this controller will not respond to
     * workspace editing. If true, it will.
     */
    public void enable(boolean enableSetting) {
	enabled = enableSetting;
    }
    
    public void handleSelectTool () {
            DataManager dataM = (DataManager) app.getDataComponent();
            dataM.setSetting("select");
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            Pane canvas = workspace.getCanvas(); 
            canvas.setCursor(Cursor.DEFAULT);
            workspace.reloadButtons();
    } 
    
    public void handleMakeRectangle () {
            DataManager dataM = (DataManager) app.getDataComponent();
            dataM.setSetting("rectangle");
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            Pane canvas = workspace.getCanvas(); 
            canvas.setCursor(Cursor.CROSSHAIR);
            handleDeselect();
            workspace.reloadButtons();
    }
    
    public void handleMakeEllipse () {
            DataManager dataM = (DataManager) app.getDataComponent();
            dataM.setSetting("ellipse");
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            Pane canvas = workspace.getCanvas(); 
            canvas.setCursor(Cursor.CROSSHAIR);
            handleDeselect();
            workspace.reloadButtons();
    }
    
    public void handleRemove () {
            DataManager dataM = (DataManager) app.getDataComponent();
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            Pane canvas = workspace.getCanvas(); 
            Shape shape = dataM.getSelectedShape();
            
            if (shape != null) {
                canvas.getChildren().remove(shape);              
                dataM.removeShape(shape);
            }
            workspace.reloadButtons();
                    app.getGUI().updateToolbarControls(false);
    }
    
    public void handleMoveFront () {
        DataManager dataM = (DataManager) app.getDataComponent();
        Shape selected = dataM.getSelectedShape();
        selected.toFront();
        dataM.moveToFront(selected);
                app.getGUI().updateToolbarControls(false);
    }
    
    public void handleMoveBack () {
        DataManager dataM = (DataManager) app.getDataComponent();
        Shape selected = dataM.getSelectedShape();
        selected.toBack();
        dataM.moveToBack(selected);
        app.getGUI().updateToolbarControls(false);
    }
    
    public void handleSnapshot () {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas(); 
        WritableImage pose = canvas.snapshot(new SnapshotParameters(), null);
        File poseImg = new File("Pose.png");
        try {
             ImageIO.write(SwingFXUtils.fromFXImage(pose, null), "png", poseImg);
        } catch (IOException ex) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(SNAPSHOT_ERROR_TITLE), props.getProperty(SNAPSHOT_ERROR_MESSAGE));
        }
        
    }
    
    public void handleFillColor (Color color) {
        DataManager dataM = (DataManager) app.getDataComponent();
        Shape selected = dataM.getSelectedShape();
        if (selected != null) {
            selected.setFill(color);
        }
                app.getGUI().updateToolbarControls(false);
    }
    
    public void handleOutlineColor (Color color) {
        DataManager dataM = (DataManager) app.getDataComponent();
        Shape selected = dataM.getSelectedShape();
       
        if (selected != null) {
            //selected.setStroke(color);
            dataM.setSelectedOutline(color);
            selected.setStroke(Color.YELLOW);
        }
                app.getGUI().updateToolbarControls(false);
    }
    
    public void handleOutlineThickness (double thickness) {
        DataManager dataM = (DataManager) app.getDataComponent();
        Shape selected = dataM.getSelectedShape();
        if (selected != null) {  
            selected.setStrokeWidth(thickness);
            dataM.setSelectedThickness(thickness);
        }
                app.getGUI().updateToolbarControls(false);
    }

    public void handleBackgroundColor(Color color) {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
                DataManager dataM = (DataManager) app.getDataComponent();
        Pane canvas = workspace.getCanvas(); 
        
        BackgroundFill bgFill = new BackgroundFill(color, CornerRadii.EMPTY, Insets.EMPTY);
        Background bg = new Background(bgFill);
        canvas.setBackground(bg);
                app.getGUI().updateToolbarControls(false);
        dataM.setBackground(color);
    }
    
    public Rectangle handleCreateRectangle(double x, double y, Color fill, Color outline, double thickness) { 
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas(); 
        Rectangle r = new Rectangle();
        r.setX(x);
        r.setY(y);
        r.setFill(fill);
        r.setStroke(outline);
        r.setStrokeWidth(thickness);
        canvas.getChildren().add(r);
        return r;
    }
    
    public Ellipse handleCreateEllipse (double x, double y, Color fill, Color outline, double thickness) { 
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas(); 
        Ellipse e = new Ellipse();
        e.setCenterX(x);
        e.setCenterY(y);
        e.setFill(fill);
        e.setStroke(outline);
        e.setStrokeWidth(thickness);
        canvas.getChildren().add(e);
        return e;
    }
    
    public void handleResizeRectangle (Rectangle rect, double width, double height) {
        rect.setWidth(width);
        rect.setHeight(height);
    }
    
    public void handleResizeEllipse (Ellipse ellipse, double radiusX, double radiusY) {
        ellipse.setRadiusX(radiusX);
        ellipse.setRadiusY(radiusY);
    }
    
    public void handleAddRectangle(Rectangle rect) {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas(); 
        DataManager dataM = (DataManager) app.getDataComponent();
        
        if (rect.getWidth() <= 0 || rect.getHeight() <= 0)
            canvas.getChildren().remove(rect);
        else {
            rect.setOnMousePressed (e -> {
                if (canvas.getCursor().equals(Cursor.DEFAULT)) {
                    handleSelectShape((Shape) rect);
                    dataM.setSelectedSceneX(e.getX());
                    dataM.setSelectedSceneY(e.getY());
                }
            });
            
            rect.setOnMouseDragged (m -> {
                if (canvas.getCursor().equals(Cursor.DEFAULT)) {
                    double moveX = m.getX() - dataM.getSelectedSceneX();
                    double moveY = m.getY() - dataM.getSelectedSceneY();
                    handleMoveShape((Shape) rect, moveX, moveY);
                    dataM.setSelectedSceneX(m.getX());
                    dataM.setSelectedSceneY(m.getY());
                }
            });
            dataM.addShape(rect);
        }
        workspace.reloadButtons();
                app.getGUI().updateToolbarControls(false);
    }
    
    public void handleAddEllipse(Ellipse ellipse) {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas(); 
        DataManager dataM = (DataManager) app.getDataComponent();
        
        if (ellipse.getRadiusX() <= 0 || ellipse.getRadiusY() <= 0)
            canvas.getChildren().remove(ellipse);
        else {
            
            ellipse.setOnMousePressed (e -> {
                if (canvas.getCursor().equals(Cursor.DEFAULT)) {
                    handleSelectShape((Shape) ellipse);
                    dataM.setSelectedSceneX(e.getX());
                    dataM.setSelectedSceneY(e.getY());
                }
            });
            
            ellipse.setOnMouseDragged (m -> {
                if (canvas.getCursor().equals(Cursor.DEFAULT)) {
                    double moveX = m.getX() - dataM.getSelectedSceneX();
                    double moveY = m.getY() - dataM.getSelectedSceneY();
                    handleMoveShape((Shape) ellipse, moveX, moveY);
                    dataM.setSelectedSceneX(m.getX());
                    dataM.setSelectedSceneY(m.getY());
                }
            });
            
            dataM.addShape(ellipse);
        }
        workspace.reloadButtons();
                app.getGUI().updateToolbarControls(false);
    }
    
    public void handleSelectShape (Shape shape) {
        DataManager dataM = (DataManager) app.getDataComponent();
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        
        //restore old selected shape
        if (dataM.getSetting().equals("select")) {
        Shape oldSelected = dataM.getSelectedShape();
        if (oldSelected != null) {
            oldSelected.setStroke(dataM.getSelectedOutline());
            oldSelected.setStrokeWidth(dataM.getSelectedThickness());
        } 
        
        //store the new values into data
        dataM.setSelectedShape(shape);
        //dataM.setSelectedFill((Color) shape.getFill());
        dataM.setSelectedOutline((Color) shape.getStroke());
        dataM.setSelectedThickness((shape.getStrokeWidth()));
        //change the values in the slider and the color picker to match that of the selected shape
        workspace.setFillPicker((Color) shape.getFill());      
        workspace.setOutlinePicker((Color) shape.getStroke());             
        workspace.setThicknessSlider(shape.getStrokeWidth());
        
        //add the yellow outline
        shape.setStroke(Color.YELLOW);
        if (shape.getStrokeWidth() <= 1)
            shape.setStrokeWidth(20);
        
        workspace.reloadButtons();
        }
    }
    
    public void handleMoveShape(Shape shape, double moveX, double moveY) {
        DataManager dataM = (DataManager) app.getDataComponent();
        if (dataM.getSetting().equals("select")) { 
                if (shape.getClass().equals(new Rectangle().getClass())) {
                    Rectangle r = (Rectangle) shape;
                    r.setX((r.getX()) + moveX);
                    r.setY((r.getY()) + moveY);
                    
                }
                else if (shape.getClass().equals(new Ellipse().getClass())) {
                    Ellipse e = (Ellipse) shape;
                    e.setCenterX((e.getCenterX()) + moveX);
                    e.setCenterY((e.getCenterY()) + moveY);
                }
            app.getGUI().updateToolbarControls(false);
        }
    }
    
    public void handleDeselect() {
        DataManager dataM = (DataManager) app.getDataComponent(); 
        Shape selectedShape = dataM.getSelectedShape();
        if (selectedShape != null) {
            selectedShape.setStroke(dataM.getSelectedOutline());
            selectedShape.setStrokeWidth(dataM.getSelectedThickness());

            dataM.setSelectedShape(null);

            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            workspace.setFillPicker(Color.BLACK);
            workspace.setOutlinePicker(Color.BLACK);
            workspace.setThicknessSlider(1.0);
        }
    }
 }
