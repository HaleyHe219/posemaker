package pm.gui;

import java.io.IOException;
import java.util.ArrayList;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import pm.PoseMaker;
import properties_manager.PropertiesManager;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import static saf.settings.AppStartupConstants.FILE_PROTOCOL;
import static saf.settings.AppStartupConstants.PATH_IMAGES;
import pm.controller.PageEditController;
import static pm.PropertyType.*;
import pm.data.DataManager;

/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class Workspace extends AppWorkspaceComponent {

    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;
    
    //Sizes
    static final int SELECT_BUTTON_WIDTH = 45;
    static final int MOVE_BUTTON_WIDTH = 100;
    static final int SNAPSHOT_BUTTON_WIDTH = 200;
    
    //CSS stuff
    static final String CLASS_HPANE = "h_boxes";
    static final String CLASS_VPANE = "v_boxes";
    static final String CLASS_MAXPANE = "max_pane";
    static final String CANVAS = "canvas";
    
    //Panes
    ArrayList<VBox> vboxes;
    ArrayList<HBox> hboxes;
    BorderPane workspaceLayout;
    VBox controlPane;
    HBox toolSelectPane;
    HBox frontBackPane;
    VBox backgroundPane;
    VBox fillPane;
    VBox outlinePane;
    VBox outlineThickPane;
    HBox capturePane;
    Pane canvasPane;
    
    //Buttons
    ArrayList<Button> Buttons;
    Button selectTool;
    Button remove;
    Button ellipse;
    Button rect;
    Button moveFront;
    Button moveBack;
    Button snapshot;
    
    ColorPicker backgroundPick;
    ColorPicker fillPick;
    ColorPicker outlinePick;
    Slider thickness;
    
    double sourceX;
    double sourceY;
    Rectangle rectInMaking;
    Ellipse ellipseInMaking;
     
    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
	// KEEP THIS FOR LATER
	app = initApp;

	// KEEP THE GUI FOR LATER
	gui = app.getGUI();
        
    // THIS HANDLES INTERACTIONS WITH PAGE EDITING CONTROLS
        PageEditController pageEditController = new PageEditController((PoseMaker) app);;
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        DataManager dataM = (DataManager) app.getDataComponent();
        
        Buttons = new ArrayList<Button>();
        hboxes = new ArrayList<HBox>();
        vboxes = new ArrayList<VBox>();
        
        workspace = new Pane();
        workspace.prefHeightProperty().bind(gui.getPrimaryScene().heightProperty());
        workspace.prefWidthProperty().bind(gui.getPrimaryScene().widthProperty());
        
        app.getGUI().getWindow().setMinHeight(900);
        app.getGUI().getWindow().setMinWidth(1000);
        
        workspaceLayout = new BorderPane();
        workspaceLayout.prefHeightProperty().bind(workspace.heightProperty());
        workspaceLayout.prefWidthProperty().bind(workspace.widthProperty());
        
        controlPane = new VBox();
        controlPane.prefHeightProperty().bind(workspaceLayout.heightProperty());
       
        toolSelectPane = new HBox(); //creat HBox with spacing
        hboxes.add(toolSelectPane);
        
        selectTool = makeButton(props.getProperty(SELECTION_TOOL_ICON.toString()), props.getProperty(SELECTION_TOOL_TOOLTIP.toString()), SELECT_BUTTON_WIDTH);
        toolSelectPane.getChildren().add(selectTool);
        Buttons.add(selectTool);
        selectTool.setOnAction(e -> {
            pageEditController.handleSelectTool();
	});
        
        remove = makeButton (props.getProperty(REMOVE_ICON.toString()), props.getProperty(REMOVE_TOOLTIP.toString()), SELECT_BUTTON_WIDTH);
        toolSelectPane.getChildren().add(remove);
        Buttons.add(remove);
        remove.setOnAction(e -> {
            pageEditController.handleRemove();
	});
        
        rect = makeButton (props.getProperty(RECTANGLE_ICON.toString()), props.getProperty(RECTANGLE_TOOLTIP.toString()), SELECT_BUTTON_WIDTH);
        toolSelectPane.getChildren().add(rect);
        Buttons.add(rect);
        rect.setOnAction(e -> {
            pageEditController.handleMakeRectangle();
	});
        
        ellipse = makeButton (props.getProperty(ELLIPSE_ICON.toString()), props.getProperty(ELLIPSE_TOOLTIP.toString()), SELECT_BUTTON_WIDTH);
        toolSelectPane.getChildren().add(ellipse);
        Buttons.add(ellipse);
        ellipse.setOnAction(e -> {
            pageEditController.handleMakeEllipse();
	});
        
        controlPane.getChildren().add(toolSelectPane);
        
        
        HBox frontBackPane = new HBox();
        hboxes.add(frontBackPane);
        
        moveFront = makeButton (props.getProperty(MOVE_FRONT_ICON.toString()), props.getProperty(MOVE_FRONT_TOOLTIP.toString()), MOVE_BUTTON_WIDTH);
        frontBackPane.getChildren().add(moveFront);
        Buttons.add(moveFront);
        moveFront.setOnAction(e -> {
            pageEditController.handleMoveFront();
	}); 
        
        moveBack = makeButton (props.getProperty(MOVE_BACK_ICON.toString()), props.getProperty(MOVE_BACK_TOOLTIP.toString()), MOVE_BUTTON_WIDTH);
        frontBackPane.getChildren().add(moveBack);
        Buttons.add(moveBack);
        moveBack.setOnAction(e -> {
            pageEditController.handleMoveBack();
	});
        controlPane.getChildren().add(frontBackPane);
        
        
        VBox backgroundPane = new VBox();
        vboxes.add(backgroundPane);
        backgroundPick = new ColorPicker(Color.GAINSBORO);
        backgroundPane.getChildren().add(new Label("Background Color"));
        backgroundPane.getChildren().add(backgroundPick);
        controlPane.getChildren().add(backgroundPane);
        backgroundPick.setOnAction(e -> {
            pageEditController.handleBackgroundColor(backgroundPick.getValue());
	});
        
        VBox fillPane = new VBox();
        vboxes.add(fillPane);
        fillPick = new ColorPicker(Color.BLACK);
        fillPane.getChildren().add(new Label("Fill Color"));
        fillPane.getChildren().add(fillPick);
        controlPane.getChildren().add(fillPane);
        fillPick.setOnAction(e -> {
            if (canvasPane.getCursor().equals(Cursor.DEFAULT))
                pageEditController.handleFillColor(fillPick.getValue());
	});
        
        VBox outlinePane = new VBox();
        vboxes.add(outlinePane);
        outlinePick = new ColorPicker(Color.BLACK);
        outlinePane.getChildren().add(new Label("Outline Color"));
        outlinePane.getChildren().add(outlinePick);
        controlPane.getChildren().add(outlinePane);
        outlinePick.setOnAction(e -> {
            if (canvasPane.getCursor().equals(Cursor.DEFAULT))
                pageEditController.handleOutlineColor(outlinePick.getValue());
	});
        
        VBox outlineThickPane = new VBox();
        vboxes.add(outlineThickPane);
        thickness = new Slider();
        thickness.setShowTickMarks(true);
        thickness.setMajorTickUnit(1);
        thickness.setValue(1);
        thickness.setMax(100);
        thickness.setMin(0);
        outlineThickPane.getChildren().add(new Label("Outline Thickness"));
        outlineThickPane.getChildren().add(thickness);
        controlPane.getChildren().add(outlineThickPane);
        thickness.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (canvasPane.getCursor().equals(Cursor.DEFAULT))
                pageEditController.handleOutlineThickness(newValue.doubleValue());
        });
        
        capturePane = new HBox();
        hboxes.add(capturePane);
        snapshot = makeButton (props.getProperty(SNAPSHOT_ICON.toString()), props.getProperty(SNAPSHOT_TOOPTIP.toString()), SNAPSHOT_BUTTON_WIDTH);
        capturePane.getChildren().add(snapshot);
        Buttons.add(snapshot);
        snapshot.setOnAction(e -> {
            pageEditController.handleSnapshot();
	});
        controlPane.getChildren().add(capturePane);
        
        //controlPane.set
        workspaceLayout.setLeft(controlPane);  
        
        canvasPane = new Pane();
        canvasPane.prefHeightProperty().bind(workspaceLayout.heightProperty());
        canvasPane.prefWidthProperty().bind(workspaceLayout.widthProperty());
        workspaceLayout.setCenter(canvasPane);
        
        canvasPane.setOnMousePressed(e -> {
            if (canvasPane.getCursor().equals(Cursor.CROSSHAIR)) {
                sourceX = e.getX();
                sourceY = e.getY();
                String setting = dataM.getSetting();
                if (setting.equals("rectangle"))
                    rectInMaking = pageEditController.handleCreateRectangle(sourceX,sourceY, fillPick.getValue(), outlinePick.getValue(), thickness.getValue());
                else if (setting.equals("ellipse"))
                    ellipseInMaking = pageEditController.handleCreateEllipse(sourceX,sourceY, fillPick.getValue(), outlinePick.getValue(), thickness.getValue());
            } 
            if (e.isControlDown()) 
                pageEditController.handleDeselect();
        });
        canvasPane.setOnMouseDragged(e -> {
            if (canvasPane.getCursor().equals(Cursor.CROSSHAIR)) {
                double shapeWidth = (e.getX()) - sourceX;
                double shapeLength = (e.getY()) - sourceY;
                String setting = dataM.getSetting();
                if (setting.equals("rectangle"))
                    pageEditController.handleResizeRectangle(rectInMaking,shapeWidth, shapeLength);
                else if (setting.equals("ellipse"))
                    pageEditController.handleResizeEllipse (ellipseInMaking, shapeWidth, shapeLength);
            }
        });
        
        canvasPane.setOnMouseReleased(e -> {
            if (canvasPane.getCursor().equals(Cursor.CROSSHAIR)) {
                String setting = dataM.getSetting();
                if (setting.equals("rectangle"))
                    pageEditController.handleAddRectangle(rectInMaking);
                else if (setting.equals("ellipse"))
                     pageEditController.handleAddEllipse(ellipseInMaking);
            } 
        });
        
	workspace.getChildren().add(workspaceLayout);
        workspaceActivated = false;
        reloadButtons();
    }
    /**
     * This function initializes a button
     * @param iconName name of png file for the icon
     * @param tooltipName tooltip texts
     * @param buttonSize size of the button
     * @return 
     *      an initialized button
     */
    public static Button makeButton (String iconName, String tooltipName, int buttonSize) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + iconName;
        Image buttonImage = new Image(imagePath);
        Button b = new Button ();
        b.setGraphic(new ImageView(buttonImage));
        Tooltip buttonToolTip = new Tooltip(tooltipName);
        b.setTooltip(buttonToolTip);
        b.setMaxWidth(buttonSize);
	b.setMinWidth(buttonSize);
	b.setPrefWidth(buttonSize);
        return b;
    }
    
    /**
     * Accessor for canvasPane
     * @return canvasPas
     */
    public Pane getCanvas () {
        return canvasPane;
    }
      
    public void setFillPicker (Color newColor) {
            fillPick.setValue(newColor);
    }
    
    public void setOutlinePicker (Color newColor) {
            outlinePick.setValue(newColor);
    }
    
    public void setThicknessSlider (double newThickness) {
            thickness.setValue(newThickness);
    }
    
    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    @Override
    public void initStyle() {
	// NOTE THAT EACH CLASS SHOULD CORRESPOND TO
	// A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
	// CSS FILE
        
        controlPane.getStyleClass().add(CLASS_MAXPANE);
        canvasPane.setId(CANVAS);
        for (HBox h : hboxes) 
	    h.getStyleClass().add(CLASS_HPANE);
        for (VBox v : vboxes) 
	    v.getStyleClass().add(CLASS_VPANE);
       
       
        
        
    }

    /**
     * This function reloads all the controls for editing tag attributes into
     * the workspace.
     */
    @Override
    public void reloadWorkspace() {
         PageEditController pageEditController = new PageEditController((PoseMaker) app);;
        
        DataManager dataM = (DataManager) app.getDataComponent();
        backgroundPick.setValue(dataM.getBackground());
        fillPick.setValue(Color.BLACK);
        outlinePick.setValue(Color.BLACK);
        thickness.setValue(1.0);
        canvasPane.setCursor(Cursor.DEFAULT);
        
        Color bgColor = dataM.getBackground();
        BackgroundFill bgFill = new BackgroundFill(bgColor, CornerRadii.EMPTY, Insets.EMPTY);
        Background bg = new Background(bgFill);
        canvasPane.setBackground(bg);
                
        
        ArrayList<Shape> shapes = dataM.getShapesArray();
        for (Shape s: shapes) {
            s.setOnMousePressed (e -> {
                if (canvasPane.getCursor().equals(Cursor.DEFAULT)) {
                    pageEditController.handleSelectShape(s);
                    dataM.setSelectedSceneX(e.getX());
                    dataM.setSelectedSceneY(e.getY());
                }
            });
            
            s   .setOnMouseDragged (m -> {
                if (canvasPane.getCursor().equals(Cursor.DEFAULT)) {
                    double moveX = m.getX() - dataM.getSelectedSceneX();
                    double moveY = m.getY() - dataM.getSelectedSceneY();
                    pageEditController.handleMoveShape(s, moveX, moveY);
                                        dataM.setSelectedSceneX(m.getX());
                    dataM.setSelectedSceneY(m.getY());
                }
            });
            canvasPane.getChildren().add(s);
        }
        reloadButtons();
    }
    
    public void reloadButtons() {
        DataManager dataM = (DataManager) app.getDataComponent();
        String setting = dataM.getSetting();
        int numShapes = canvasPane.getChildren().size();
        
        if (dataM.getSelectedShape() == null)
            remove.setDisable(true);
        
        if (numShapes == 0) {
            remove.setDisable(true);
            //selectTool.setDisable(true);
            moveFront.setDisable(true);
            moveBack.setDisable(true);
            return;
        }
        if (setting != null) {
        switch (setting) {
            case "rectangle":
            case "ellipse":
                moveFront.setDisable(true);
                moveBack.setDisable(true);
                fillPick.setDisable(false);
                outlinePick.setDisable(false);
                thickness.setDisable(false);
                remove.setDisable(true);
                if (numShapes > 0)
                    selectTool.setDisable(false);
                break;
            case "remove":
                moveFront.setDisable(true);
                moveBack.setDisable(true);
                fillPick.setDisable(true);
                outlinePick.setDisable(true);
                thickness.setDisable(true);
                break;
            case "select":
                moveFront.setDisable(false);
                moveBack.setDisable(false);
                fillPick.setDisable(false);
                outlinePick.setDisable(false);
                thickness.setDisable(false);
                remove.setDisable(false);
                break;
            default:
                break;
        }
        }
    }
}
